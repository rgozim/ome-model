# Contributing features and improvements

The [README](README.md) gives instructions for testing your code
before opening a merge request.  Please read this first.

## Local setup

* Fork the GitLab repository.

## Making and submitting a merge request

* Create a branch for your work based on the current `master` branch.
* Make your commits, test your changes locally as per the
  [README](README.md), and open a merge request.
* Make sure to include details of the problem you are fixing and how
  to test your changes in the merge request description.
* You may need to submit some test data for changes which require
  specific data to test.

## Review

* The merge request will be initially tested by a GitLab pipeline.
  Check all the pipeline stages complete successfully.
* If the pipeline fails, make any required changes to fix the failures
  and push the changes; it will be automatically retested.
* Following code review, please address any feedback provided.
* The changes will be merged once review and testing are complete.
