5.7.0 (2018-09-16)
------------------

The main focus of this release is a set of changes to the xsd-fu code
generator and language templates which permit use with Python 3 as
well as Python 2.  These changes also improve the generated code by
ordering the fields and methods in the order of declaration in the
OME-XML schema (previously the order was undefined).  Additional
generator changes significantly improve C++ build times.

Minimum language versions have been updated to C++14 and Java 8, with
maven updates to support Java 10 and 11.

The source repository has been copied to
[GitLab](https://gitlab.com/codelibre/ome-model), and uses
[GitLab CI](https://gitlab.com/codelibre/ome-model/pipelines) for CI
testing and
[GitLab Issues](https://gitlab.com/codelibre/ome-model/issues)
for issue tracking.  The GitLab CI "generator" and "changes" stages
replace the Jenkins jobs previously used for reviewing generated
source changes.  GitLab CI testing covers Java and C++ using Python 3
and Python 2 for code generation.

Documentation:

* Improve documentation link checker
  [#70](https://github.com/ome/ome-model/pull/70)
* Correct OME-TIFF documentation link
  [#76](https://github.com/ome/ome-model/pull/76)
* README: Update links to refer to new GitLab and maven central locations
  [!102](https://gitlab.com/codelibre/ome-model/merge_requests/102)

Build:

* Maven plugin and build updates
  [#74](https://github.com/ome/ome-model/pull/74)
* Maven plugin and build updates for Java 10
  [#80](https://github.com/ome/ome-model/pull/80)
* Require C++14 for ome-xml C++ library
  [#75](https://github.com/ome/ome-model/pull/75)
* CMake documentation generation improvements
  [!86](https://gitlab.com/codelibre/ome-model/merge_requests/86)
* Update maven group ID and add CI testing with Java 10 and Java 8
  [!87](https://gitlab.com/codelibre/ome-model/merge_requests/87)
* Require Java 1.8 as the minimum supported version
  [!100](https://gitlab.com/codelibre/ome-model/merge_requests/100)

Code generation:

* Add `createRotationTransform()` static method to `AffineTransform`
  [#77](https://github.com/ome/ome-model/pull/77)
* xsd-fu: Aggregate generated C++ source files into single source files for
  model and enums
  [#79](https://github.com/ome/ome-model/pull/79)
* xsd-fu: generateDS: Preserve element and attribute ordering
  [!93](https://gitlab.com/codelibre/ome-model/merge_requests/93)
* xsd-fu: Make templates compatible with Python 3
  [!94](https://gitlab.com/codelibre/ome-model/merge_requests/94)
* xsd-fu: Replace `odict` with `OrderedDict` from collections
  [!95](https://gitlab.com/codelibre/ome-model/merge_requests/95)
* xsd-fu: Modernise genshi and generateDS to work with Python 3 or Python 2.7
  [!96](https://gitlab.com/codelibre/ome-model/merge_requests/96)
* xsd-fu: Use stable metadata store method ordering
  [!97](https://gitlab.com/codelibre/ome-model/merge_requests/97)
* xsd-fu: Make variable diagnostic strings debug output
  [!98](https://gitlab.com/codelibre/ome-model/merge_requests/98)
* Add support for building with Python 3 as well as Python 2
  [!90](https://gitlab.com/codelibre/ome-model/merge_requests/90)
* Remove erroneous warnings when processing annotation references
  [!91](https://gitlab.com/codelibre/ome-model/merge_requests/91)
* xsd-fu: Order C++ `AllModelObjects` aggregated classes alphabetically
  [!101](https://gitlab.com/codelibre/ome-model/merge_requests/101)

GitLab CI:

* Add GitLab CI configuration
  [!82](https://gitlab.com/codelibre/ome-model/merge_requests/82)
* GitLab CI documentation generation uses ubuntu build for dependencies
  [!83](https://gitlab.com/codelibre/ome-model/merge_requests/83)
* GitLab CI does not duplicate artifacts for pages stage
  [!84](https://gitlab.com/codelibre/ome-model/merge_requests/84)
* GitLab CI pages stage reuses documentation build artifact
  [!85](https://gitlab.com/codelibre/ome-model/merge_requests/85)
* GitLab CI generates and archives xsd-fu-generated sources
  [!89](https://gitlab.com/codelibre/ome-model/merge_requests/89)
* GitLab CI compares generated sources with previous merge build
  [!92](https://gitlab.com/codelibre/ome-model/merge_requests/92)

5.6.2 (2018-01-25)
------------------

* Report numerical value domain errors more clearly
  [#68](https://github.com/ome/ome-model/pull/68)

5.6.1 (2018-01-17)
------------------

Documentation updates:

* Update documentation links to use new docs.openmicroscopy.org URLs
  [#61](https://github.com/ome/ome-model/pull/61)
* Clarify and update OME-XML documentation, including removing references to
  OME-XML as a data model [#64](https://github.com/ome/ome-model/pull/64)
* Fix Leica Microsystems URL [#63](https://github.com/ome/ome-model/pull/63)
  [#66](https://github.com/ome/ome-model/pull/66)

5.6.0 (2017-12-01)
------------------

* Use C++11 `<thread>` and `<mutex>`
  [#45](https://github.com/ome/ome-model/pull/45)
* Re-add support for Boost 1.53
  [#51](https://github.com/ome/ome-model/pull/51)
* Add support for Boost 1.65.1 [#52](https://github.com/ome/ome-model/pull/52)
* Update CMake requirements to 3.4
  [#49](https://github.com/ome/ome-model/pull/49)
* CMake: use CMP0067 to enable standard setting in feature tests
  [#47](https://github.com/ome/ome-model/pull/47)
* CMake: remove REQUIRED from exported configuration
  [#54](https://github.com/ome/ome-model/pull/54)
* CMake: remove find modules distributed with CMake
  [#59](https://github.com/ome/ome-model/pull/59)

Documentation updates:

* Update OME website URLs and copyright years
  [#48](https://github.com/ome/ome-model/pull/48)
* Add versioning note and update README
  [#50](https://github.com/ome/ome-model/pull/50)
* Add OMERO pyramid specification to the documentation
  [#55](https://github.com/ome/ome-model/pull/55)
* Fix miscellaneous links [#56](https://github.com/ome/ome-model/pull/56)
  [#57](https://github.com/ome/ome-model/pull/57)
  [#58](https://github.com/ome/ome-model/pull/58)
  [#60](https://github.com/ome/ome-model/pull/60)

5.5.7 (2017-07-11)
------------------

* CMake: move options inclusion after project call
  [#42](https://github.com/ome/ome-model/pull/42)

Documentation updates:

* Use new OME Sphinx theme [#44](https://github.com/ome/ome-model/pull/44)

5.5.6 (2017-06-15)
------------------

* xsd-fu: correct C++ model object template
  [#41](https://github.com/ome/ome-model/pull/41)

Documentation updates:

* Use master as the source branch
  [#40](https://github.com/ome/ome-model/pull/40)

5.5.5 (2017-06-15)
------------------

Released as 5.5.6

5.5.4 (2017-06-09)
------------------

* Update OME-XML API reference including use of deleted methods
  [#38](https://github.com/ome/ome-model/pull/38)
* CMake: use C++ standard variable as documented
  [#39](https://github.com/ome/ome-model/pull/39)

5.5.3 (2017-05-24)
------------------

* Increase OMECommon and OMECompat requirements
  [#37](https://github.com/ome/ome-model/pull/37)
* CMake: add support for Boost 1.64
  [#35](https://github.com/ome/ome-model/pull/35)

Documentation updates:

* Fix BigTIFF links [#34](https://github.com/ome/ome-model/pull/34)

5.5.2 (2017-02-28)
------------------

Documentation updates:

* Add documentation links to the README
  [#32](https://github.com/ome/ome-model/pull/32)
* Reorganize OME-TIFF samples [#31](https://github.com/ome/ome-model/pull/31)
* Add BBBC and MitoCheck datasets to the OME-TIFF samples page
  [#33](https://github.com/ome/ome-model/pull/33)

5.5.1 (2017-02-16)
------------------

* OME-XML C++: insert missing links [#30](https://github.com/ome/ome-model/pull/30)

5.5.0 (2017-02-10)
------------------

* Enable C++ 11 support [#10](https://github.com/ome/ome-model/pull/10)
* Add minimal C++ 11 features [#25](https://github.com/ome/ome-model/pull/25)
* OME-XML performance improvements
  [#28](https://github.com/ome/ome-model/pull/28)
* CMake: obtain release version from Maven
  [#29](https://github.com/ome/ome-model/pull/29)

5.4.0 (2017-01-05)
------------------

* Fix ROI transforms [#14](https://github.com/ome/ome-model/pull/14)
* Add transform example to ROI OME-XML sample
  [#13](https://github.com/ome/ome-model/pull/13)
* Cleanup POM files [#11](https://github.com/ome/ome-model/pull/11)
  [#15](https://github.com/ome/ome-model/pull/15)
  [#16](https://github.com/ome/ome-model/pull/16)
  [#18](https://github.com/ome/ome-model/pull/18)
* Support conversion checks for Java and C++
  [#12](https://github.com/ome/ome-model/pull/12)
* CMake: drop gtest source building support
  [#9](https://github.com/ome/ome-model/pull/9)
* CMake: add support for Boost 1.63
  [#21](https://github.com/ome/ome-model/pull/21)
* CMake: change project name to ome-model
  [#20](https://github.com/ome/ome-model/pull/20)


5.3.1 (2016-11-03)
------------------

* Use ome-common 5.3.1 [#7](https://github.com/ome/ome-model/pull/7)
* Fix schema publication scripts [#6](https://github.com/ome/ome-model/pull/6)

5.3.0 (2016-10-25)
------------------

* Decouple the `ome-model` component from the Bio-Formats source code
* Drop `xsd-fu` code-generation tool as a Java module
  [#2](https://github.com/ome/ome-model/pull/2)
* Add build support for Maven and CMake
  [#1](https://github.com/ome/ome-model/pull/1)
